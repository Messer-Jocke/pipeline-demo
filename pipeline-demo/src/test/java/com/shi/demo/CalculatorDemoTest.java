package com.shi.demo;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class CalculatorDemoTest {

	private Calculator calc = new Calculator();
	
	@Test
	public void simpleAdd() {
		int result = calc.add(5, 7);
		
		assertThat(result).isEqualTo(12);
	}
	
	@Test
	public void failingTest() {
		int result = calc.add(2, 3);
		
		assertThat(result).isEqualTo(5);
	}
}
