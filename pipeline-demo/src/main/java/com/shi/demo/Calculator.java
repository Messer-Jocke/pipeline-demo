package com.shi.demo;

public class Calculator {

	public int add(int summand1, int summand2) {
		return summand1 + summand2;
	}
	
	public int multiply(int m1, int m2) {
		return m1 * m2;
	}
}
